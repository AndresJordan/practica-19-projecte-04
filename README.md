# PRACTICA 19 - PROJECTE 04 #
Codi del projecte en el repositori GitHub, indica la URL en el comentari de la tramesa Moodle.
Prova del codi dins una VM arrencada amb Vagrant.
Aquest resultat (per fer les proves, fes una còpia més reduïda del fitxer de logs perquè si no cada prova triga massa).
## Requisitos previos

* Instalar Linux
* Instalar Vagrant

## Pasos para la instalación

Para empezar la instalación hay que descargar los archivos necesarios del respositorio de Bitbucket.

```
git clone https://AndresJordan@bitbucket.org/AndresJordan/practica-19-projecte-04.git
```

Este respositorio contiene los ficheros necesarios en shellclass/localusers/:

```
init-vagrant.sh
show-attackers.sh
syslog-sample
```

### init-vagrant.sh

Script que se ejecuta para iniciar el .box y la VM.

### show-attackers.sh

Script que contiene el código. Esta situado en /home/vagrant/.

### syslog-sample

Fichero que contiene el log de un sistema. Esta situado en /home/vagrant/.

## Pasos para la ejecución

Situados en el directorio shellclass/localusers/.

```
./init-vagrant.sh
```

## VirtualBox

```
User: vagrant
Password: vagrant
```
