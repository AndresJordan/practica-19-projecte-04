#!/bin/bash
# Para que funcione correctamente el script tenemos que instalar geoip-bin
# Inicializamos la array donde se almacenaran las IP's con su contador
declare -A arrIPs
# Leemos cada IP del fichero
while read linea; do
                # Si la en la array la IP no ha sido seteada inicializamos su valor
                if [[ ! -v "arrIPs[$linea]" ]]; then
                        arrIPs[$linea]=1
                # En caso que ya haya sido inicializada incrementamos su valor
                else
                     arrIPs[$linea]=$((${arrIPs[$linea]}+1))
                fi
# Filtramos el fichero por la palabra Failed y con NF obtenemos el último campo de la linea
done < <(awk '/Failed/ {printf "%s\n",$(NF-3)}' $1)
# Mostramos la cabecera
echo "Count,IP,Location"
# Recorremos la array para mostrar su valor
for keyIP in "${!arrIPs[@]}"; do
        # Comprobamos que la IP haya aparecido más de 10 veces
        if [[ "arrIPs[$keyIP]" -gt 10 ]]; then
                # Obtenemos la lozalización de la IP y removemos el primer espacio en blanco
                localizacionIP=$(geoiplookup $keyIP | awk 'BEGIN{FS=","}{printf "%s\n",$2}' | sed 's/^ *//')
                echo "${arrIPs[$keyIP]},$keyIP,$localizacionIP"
        fi
# Ordenamos numericamente pero con texto y a la inversa
done | sort -Vr